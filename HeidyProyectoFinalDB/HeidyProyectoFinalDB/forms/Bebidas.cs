﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class Bebidas : Form
    {
        private List<clases.Bebidas> bebidas;

        public Bebidas()
        {
            InitializeComponent();
            this.dgvBebidasReset();
        }

        private void dgvBebidasReset()
        {
            bebidas = new clases.ClaBebidas().Consultar();
            this.dgvBebidas.DataSource = bebidas.ToList<clases.Bebidas>();
        }

        private void Buscar()
        {
            var res = this.bebidas.FindAll(dat => 
            dat.NombreBebida.ToLower().Contains(this.txtFiltrar.Text.ToLower()) || 
            dat.TipoBebidas.ToLower().Contains(this.txtFiltrar.Text.ToLower()) ||
            dat.CategoriaBebida.ToLower().Contains(this.txtFiltrar.Text.ToLower()));
            this.dgvBebidas.DataSource = res;
        }

        private void txtFiltrar_KeyUp(object sender, KeyEventArgs e)
        {
            this.Buscar();
        }
    }
}
