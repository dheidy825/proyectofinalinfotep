﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeidyProyectoFinalDB.forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class FormLoguin : Form
    {
        public FormLoguin()
        {
            InitializeComponent();
        }

        public void Entrar()
        {
            string exp = @"\A[a-zA-Z0-9]+";
            if(new Regex(exp).IsMatch(this.txtUsuario.Text))
            {
                if(new Regex(exp).IsMatch(this.txtClave.Text))
                {
                    if(new clases.Login().isUser(this.txtUsuario.Text, this.txtClave.Text))
                    {
                        this.Hide();
                        new FormMain().Show();
                    }
                    else
                    {
                        MessageBox.Show(
                            null,
                            "Usuario y/o clave invalidos.",
                            "Error al inciar sesión",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error
                        );
                    }
                }
                else
                {
                    clases.ClaNotificaciones.CampoVacio("Clave");
                }
            }
            else
            {
                clases.ClaNotificaciones.CampoVacio("Usuario");
            }
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void buttEntrar_Click(object sender, EventArgs e)
        {
            this.Entrar();
        }

        private void buttSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
