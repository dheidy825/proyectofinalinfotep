﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HeidyProyectoFinalDB.forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class FormMain : Form
    {
        

        public static Usuario usuario = null;
        public static Bebidas bebidas = null;
        public static Factura factura = null;
        public static Inventario inventario = null;
        private static AboutBox1 aboutBox = null;

        private List<Form> form;


        public FormMain()
        {
            InitializeComponent();
            this.Saludar();
            this.form = new List<Form>()
            {
                new Usuario(),
                new Bebidas(),
                new Factura(),
                new Inventario(),
                new AboutBox1()
            };
            this.timer1.Start();
            this.Ocultar(0);
        }

        public void Reloj()
        {
            this.toolStripLabelReloj.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }

        public void Saludar()
        {
            this.toolStripLabelUsuario.Text = $"Bienvenido(a): { clases.Sesion.Nombre}, {clases.Sesion.Apellido}, ({clases.Sesion.Usuario})";
        }

        /// <summary>
        /// Oculta los formularios del menu principal.
        /// </summary>
        private void Ocultar(int pos)
        {
            if (this.Controls[2].Controls.Count > 0)
                this.Controls[2].Controls.Clear();

            this.form[pos].MdiParent = this;
            this.form[pos].Show();
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ocultar(0);
        }

        private void bebidasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ocultar(1);
        }

        private void facturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ocultar(2);
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ocultar(3);
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Ocultar(4);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Reloj();
        }
    }
}
