﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class Factura : Form
    {
        private List<clases.Bebidas> bebidas;
        private List<clases.Bebidas> facturabebidas = new List<clases.Bebidas>();

        public Factura()
        {
            InitializeComponent();
            this.dgvFacturaReset();
        }

        private void dgvFacturaReset()
        {
            bebidas = new clases.ClaBebidas().Consultar();
            this.dgvFacturas.DataSource = bebidas.ToList();
        }

        private void dgvFacturShow()
        {
            this.dgvFactura.DataSource = this.facturabebidas.ToList();
        }

        private void Buscar()
        {
            var res = this.bebidas.FindAll(dat =>
            dat.NombreBebida.ToLower().Contains(this.txtFiltrar.Text.ToLower()) ||
            dat.TipoBebidas.ToLower().Contains(this.txtFiltrar.Text.ToLower()) ||
            dat.CategoriaBebida.ToLower().Contains(this.txtFiltrar.Text.ToLower()));
            this.dgvFacturas.DataSource = res;
        }

        private void txtFiltrar_KeyUp(object sender, KeyEventArgs e)
        {
            this.Buscar();
        }

        private void dgvFacturas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.facturabebidas.Add((clases.Bebidas)this.dgvFacturas.CurrentRow.DataBoundItem);
            this.dgvFacturShow();
        }

        private void dgvFactura_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var value = (clases.Bebidas)this.dgvFactura.CurrentRow.DataBoundItem;
            for (int i=0; i<this.facturabebidas.Count; i++)
            {
                if (this.facturabebidas[i].Equals(value))
                {
                    this.facturabebidas.RemoveAt(i);
                    this.dgvFacturShow();
                    break;
                }
            }
        }

        private void Facturar()
        {
            for(int i=0; i<this.facturabebidas.Count; i++)
            {
                new clases.ClaDetalleFactura(
                    this.facturabebidas[i].ID,
                    clases.Sesion.ID,
                    1
                ).Agregar();
            }
            this.facturabebidas.Clear();
            this.dgvFacturShow();
            clases.ClaNotificaciones.Correcto();
        }

        private void BttFacturar_Click(object sender, EventArgs e)
        {
            this.Facturar();
        }
    }
}
