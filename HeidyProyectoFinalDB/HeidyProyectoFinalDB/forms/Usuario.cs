﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class Usuario : Form
    {
        private List<clases.Cusuario.Usuario> usuarios;
        private clases.Cusuario.Usuario usuario;

        public Usuario()
        {
            InitializeComponent();
            this.dgvMostrarDatos();
        }

        private void dgvMostrarDatos()
        {
            this.usuarios = new clases.Cusuario().Consulta();
            this.dgvUsuario.DataSource = usuarios;
            this.dgvUsuario.Columns[0].Visible = false;
            this.Limpiar();
        }

        private bool isValid()
        {
            bool answer = false;
            string nombre = this.txtNombre.Text;
            string apellido = this.txtApellido.Text;
            string telefono = this.txtTelefono.Text;
            string cedula = this.txtCedula.Text;
            string direccion = this.txtDireccion.Text;
            string correo = this.txtCorreo.Text;
            string usuario = this.txtUsuario.Text;
            string clave = this.txtContrasena.Text;

            bool isValid = nombre != string.Empty && apellido != string.Empty && telefono != string.Empty &&
                cedula != string.Empty && direccion != string.Empty && correo != string.Empty &&
                usuario != string.Empty && clave != string.Empty;

            if (isValid)
            {
                string exp = @"\A[0-9]{3}\-?[0-9]{7}\-?[0-9]{1}\Z";
                if (new Regex(exp).IsMatch(cedula))
                {
                    exp = @"\A[0-9]{3}\-?[0-9]{3}\-?[0-9]{4}\Z";
                    if (new Regex(exp).IsMatch(telefono))
                    {
                        answer = true;
                    }
                    else
                    {
                        MessageBox.Show(null, "Formato de Teléfono Incorrecto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show(null, "Formato de Cédula Incorrecto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show(null, "Completa los Campos Vacios.", "Campos Vacios", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return answer;
        }

        private void Agregar()
        {
            if(this.isValid())
            {
                new clases.Cusuario(
                    this.txtUsuario.Text,
                    this.txtContrasena.Text,
                    this.txtNombre.Text,
                    this.txtApellido.Text,
                    this.txtCedula.Text,
                    this.txtTelefono.Text,
                    this.txtCorreo.Text,
                    this.txtDireccion.Text
                ).Agregar();
                this.dgvMostrarDatos();
                clases.ClaNotificaciones.Correcto();
            }
                    
        }

        private void pbAgregar_Click(object sender, EventArgs e)
        {
            this.Agregar();
        }

        private void MostrarBotones(bool state)
        {
            this.pbBorrar.Enabled = state;
            this.pbAgregar.Enabled = !state;
            this.pbActualizar.Enabled = state;
            this.pbCancelar.Enabled = state;
        }

        private void dgvUsuario_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            usuario = (clases.Cusuario.Usuario)this.dgvUsuario.CurrentRow.DataBoundItem;
            // llenando textbox
            this.LlenarCampos();
            this.MostrarBotones(true);
        }

        private void LlenarCampos()
        {
            this.txtApellido.Text = usuario.apellido;
            this.txtNombre.Text = usuario.nombre;
            this.txtCedula.Text = usuario.cedula;
            this.txtContrasena.Text = "******";
            this.txtCorreo.Text = usuario.correo;
            this.txtDireccion.Text = usuario.direccion;
            this.txtTelefono.Text = usuario.telefono;
            this.txtUsuario.Text = usuario.usuario;
        }
        private void Buscar()
        {
            var resultado = this.usuarios.Where(x => x.nombre.ToLower().Contains(this.textBuscar.Text) || x.apellido.ToLower().Contains
            (this.textBuscar.Text)).ToList();
            this.dgvUsuario.DataSource = resultado;
        }

        private void Limpiar()
        {
            this.usuario = new clases.Cusuario.Usuario();
            this.LlenarCampos();
            this.MostrarBotones(false);
        }

        private void Eliminar()
        {
            var ans = clases.ClaNotificaciones.DeseaEliminar();
            if(ans == DialogResult.Yes)
            {
                new clases.Cusuario(usuario.id).Eliminar();
                this.dgvMostrarDatos();
                MessageBox.Show(null, "Registro Eliminado!", "Borrado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void pbBorrar_Click(object sender, EventArgs e)
        {
            this.Eliminar();
        }

        private void pbCancelar_Click(object sender, EventArgs e)
        {
            this.Limpiar();
        }

        private void Actualizar()
        {
            if (this.isValid())
            {
                new clases.Cusuario(
                    usuario.id,
                    this.txtUsuario.Text,
                    this.txtContrasena.Text,
                    this.txtNombre.Text,
                    this.txtApellido.Text,
                    this.txtCedula.Text,
                    this.txtTelefono.Text,
                    this.txtCorreo.Text,
                    this.txtDireccion.Text
                ).Actualizar();
                this.dgvMostrarDatos();
                clases.ClaNotificaciones.Correcto();
            }
        }

        private void pbActualizar_Click(object sender, EventArgs e)
        {
            this.Actualizar();
        }

        private void textBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            this.Buscar();
        }
    }
}
