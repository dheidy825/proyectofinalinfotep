﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeidyProyectoFinalDB.forms
{
    public partial class Inventario : Form
    {

        private List<clases.claInventario> inventarios;
        private List<clases.claInventario> inventario = new List<clases.claInventario>();

        public Inventario()
        {
            InitializeComponent();
        }
        
        

        private bool isValid()
        {
           
            return this.txtCantidad.Text.Length > 0 &&
                this.txtCategoria.Text.Length > 0 &&
                this.txtCompania.Text.Length > 0 &&
                this.txtDireccion.Text.Length > 0 &&
                this.txtPrecio.Text.Length > 0 &&
                this.txtProducto.Text.Length > 0 &&
                this.txtProveedor.Text.Length > 0 &&
                this.txtTelefono.Text.Length > 0 &&
                this.txtTipo.Text.Length > 0;

        }

        private void Agregar()
        {
            if (this.isValid())
            {
                new clases.claInventario(

                    ).Agregar();
            }
        }

        private void pbAgregar_Click(object sender, EventArgs e)
        {
            this.Agregar();
        }
    }
}
