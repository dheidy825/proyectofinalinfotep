﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class claInventario : ClaBebidas
    {

        private int ID;
        private int idEmpleado;
        private int CantidadProducto;
        private string NombreProveedor;
        private string CompaniaProveedora;
        private string TelefonoProveedor; 
        private string DireccionProveedor;
        private string table = "inventario";

        public claInventario() 
        { 

        }

        public claInventario(int iD)
        {
           this.ID = iD;
        }

        public claInventario(int idEmpleado, int cantidadProducto, string nombreProveedor, string companiaProveedora, string telefonoProveedor, string direccionProveedor, string tipo, string nombre, string categoria, float precio) : base(tipo,nombre,categoria,precio)
        {
            this.idEmpleado = idEmpleado;
            CantidadProducto = cantidadProducto;
            NombreProveedor = nombreProveedor;
            CompaniaProveedora = companiaProveedora;
            TelefonoProveedor = telefonoProveedor;
            DireccionProveedor = direccionProveedor;
        }

        public claInventario(int iD, int idEmpleado, int idBebidas, int cantidadProducto, string nombreProveedor, string companiaProveedora, string telefonoProveedor, string direccionProveedor, string tipo, string nombre, string categoria, float precio) : base(idBebidas,tipo, nombre, categoria, precio)
        {
            this.ID = iD;
            this.idEmpleado = idEmpleado;
            CantidadProducto = cantidadProducto;
            NombreProveedor = nombreProveedor;
            CompaniaProveedora = companiaProveedora;
            TelefonoProveedor = telefonoProveedor;
            DireccionProveedor = direccionProveedor;
        }

        public new void Agregar()
        {
            base.Agregar();
            string sql = $"INSERT INTO " +
                $"{this.table}" +
                    $"(idEmpleado,idBebidas,cantidaproducto,nombreprovedor,companiaproveedor,telefonoproveedor,direccionproveedor) " +
                $"VALUE" +
                    $"(" +
                        $"{this.idEmpleado}, " +
                        $"{base.id}," +
                        $"{this.CantidadProducto}," +
                        $"'{this.NombreProveedor}'," +
                        $"'{this.CompaniaProveedora}'," +
                        $"'{this.TelefonoProveedor}'," +
                        $"'{this.DireccionProveedor}'" +
                    $")";
            base.EjecutarAEI(sql);
        }
    
        public new List<Inventario> Consultar()
        {
            List<Inventario> inventario = new List<Inventario>();
            string sql = $" SELECT * FOM {this.table} WHERE eliminador= FALSE";
            var query = base.LeerDatos(sql);
            while (query.Read())
            {
                inventario.Add(
                    new Inventario()
                    {
                        ID = query.GetInt32("id"),
                        idEmpleado = query.GetInt32("idempleado"),
                        idBebidas = query.GetInt32("idbebidas"),
                        TipoBebida = query.GetString("tipobebidas"),
                        NombreBebidas = query.GetString("nombrebebidas"),
                        categoriaBebida = query.GetString("categoriabebida"),
                        PrecioBebidas = query.GetString("preciobebidas"),
                        MarcaBebidas = query.GetString("marcabebidas"),
                        CantidadProducto = query.GetInt32("cantidadproducto"),
                        NombreProveedor = query.GetString("nombreproveedor"),
                        CompaniaProveedora = query.GetString("companiaproveedora"),
                        TelefonoProveedor = query.GetString("telefonoproveedor"),
                        DireccionProveedor = query.GetString("direccionproveedor")
                    }
                );
            }
            base.DesconectarMysql();
            return inventario;
        }

        public new void Eliminar()
        {
            string sql = $"UPDATE {this.table} SET eliminado=TRUE WHERE id={this.id}";
            base.EjecutarAEI(sql);
        }

        public new void Actualizar()
        {
            string sql = $"UPDATE " +
                   $"{this.table} " +
               $"SET " +
                   $"cantidadproducto='{this.CantidadProducto}', " +
                   $"nombreproveedor='{this.NombreProveedor}', " +
                   $"companiaproveedora='{this.CompaniaProveedora}' " +
                   $"telefonoproveedor='{this.TelefonoProveedor}' " +
                   $"direccionproveedor='{this.DireccionProveedor}' " +
               $"WHERE " +
                   $"id={this.id}";
            base.EjecutarAEI(sql);
        }
    }

    public class Inventario
    {

        public int ID { set; get; }
        public int idEmpleado { set; get; }
        public int idBebidas { set; get; }
        public string TipoBebida { set; get; }
        public string NombreBebidas { set; get; }
        public string categoriaBebida { set; get; }
        public string PrecioBebidas { set; get; }
        public string MarcaBebidas { set; get; }
        public int CantidadProducto { set; get; }
        public string NombreProveedor { set; get; }
        public string CompaniaProveedora { set; get; }
        public string TelefonoProveedor { set; get; }
        public string DireccionProveedor { set; get; }

    }
}
