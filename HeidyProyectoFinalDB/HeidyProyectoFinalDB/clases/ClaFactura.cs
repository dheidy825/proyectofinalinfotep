﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class ClaFactura : ConexionDBMysql
    {
        protected int idFactura;
        private int idBebidas ;
        private int idEmpleado ;
        private string table = "factura";

        public ClaFactura(int idBebidas, int idEmpleado)
        {
            this.idBebidas = idBebidas;
            this.idEmpleado = idEmpleado;
        }

        public void Agregar()
        {
            string sql = $"INSERT INTO {this.table}(idBebidas,idEmpleado) " +
                $"VALUE({this.idBebidas},{this.idEmpleado})";
            base.EjecutarAEI(sql);
            this.idFactura = base.UltimoIDInsetado;
        }
    }
}
