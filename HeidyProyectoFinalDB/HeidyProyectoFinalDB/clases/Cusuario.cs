﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class Cusuario : ConexionDBMysql
    {
        private int id;
        private string usuario;
        private string Clave;
        private string Nombre;
        private string Apellido;
        private string Cedula ;
        private string Telefono ;
        private string Correo ;
        private string Direccion;
        private string table = "empleado";

        public Cusuario() { }

        public Cusuario(int id) {
            this.id = id;
        }

        public Cusuario(string usuario, string clave, string nombre, string apellido, string cedula, string telefono, string correo, string dirreccion)
        {
            this.usuario = usuario;
            Clave = clave;
            Nombre = nombre;
            Apellido = apellido;
            Cedula = cedula;
            Telefono = telefono;
            Correo = correo;
            Direccion = dirreccion;
        }

        public Cusuario(int id, string usuario, string clave, string nombre, string apellido, string cedula, string telefono, string correo, string direccion) : this(id)
        {
            this.usuario = usuario;
            Clave = clave;
            Nombre = nombre;
            Apellido = apellido;
            Cedula = cedula;
            Telefono = telefono;
            Correo = correo;
            Direccion = direccion;
        }
        

        public void Agregar()
        {
            string sql = $"INSERT INTO " +
                $"{this.table}" +
                    $"(usuario,clave,nombre,apellido,cedula,telefono,correo,direccion) " +
                $"VALUE(" +
                    $"'{this.usuario}', " +
                    $"MD5('{this.Clave}'), " +
                    $"'{this.Nombre}', " +
                    $"'{this.Apellido}', " +
                    $"'{this.Cedula}', " +
                    $"'{this.Telefono}'," +
                    $"'{this.Correo}'," +
                    $"'{this.Direccion}'" +
                $")";
            base.EjecutarAEI(sql);
        }

        public void Eliminar()
        {
            string sql = $"UPDATE {this.table} SET eliminado=TRUE WHERE id={this.id}";
            base.EjecutarAEI(sql);
        }

        public List<Usuario> Consulta()
        {
           List<Usuario> usuario = new List<Usuario>();
            string sql = $"SELECT * FROM {this.table} WHERE eliminado=FALSE";
            var query = base.LeerDatos(sql);
            while (query.Read())
            {
                usuario.Add(
                    new Usuario()
                    {
                        id = query.GetInt32("id"),
                        usuario = query.GetString("usuario"),
                        nombre = query.GetString("nombre"),
                        apellido = query.GetString("apellido"),
                        cedula = query.GetString("cedula"),
                        telefono = query.GetString("telefono"),
                        correo = query.GetString("correo"),
                        direccion = query.GetString("direccion")
                    }
                );
            }

            return usuario;
        }

        public void Actualizar()
        {
            this.Clave = this.Clave.Equals("******") ? $"(SELECT clave FROM {this.table} WHERE id={this.id})" : $"MD5('{this.Clave}')";
            string sql = $"UPDATE " +
                    $"{this.table} " +
                $"SET " +
                    $"nombre='{this.Nombre}'," +
                    $"apellido='{this.Apellido}'," +
                    $"cedula='{this.Cedula}'," +
                    $"usuario='{this.usuario}'," +
                    $"clave={this.Clave}," +
                    $"telefono='{this.Telefono}'," +
                    $"correo='{this.Correo}'," +
                    $"direccion='{this.Direccion}' " +
                $"WHERE " +
                    $"id={this.id}";
            base.EjecutarAEI(sql);
        }

        public class Usuario
        {
            public int id { set; get; }
            public string usuario { set; get; }
            public string nombre { set; get; }
            public string apellido { set; get; }
            public string cedula { set; get; }
            public string telefono { set; get; }
            public string correo { set; get; }
            public string direccion { set; get; }
        }
    }
}
