﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HeidyProyectoFinalDB.clases
{
    class ClaNotificaciones
    {
        public static void Correcto()
        {
            System.Windows.Forms.MessageBox.Show("Realizado con Exicto.", "Proceso correcto", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
        }
       
        public static void Error()
        {
            System.Windows.Forms.MessageBox.Show("Error .", "Error en el proceso", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }
        
        public static void CampoVacio(string campo)
        {
            System.Windows.Forms.MessageBox.Show($"El campo {campo} està vacìo.", "Campo vacìo", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
       
        public static DialogResult DeseaEliminar()
        {
            return System.Windows.Forms.MessageBox.Show(" Esta seguro que desea eliminar este registro", "Confirmar", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
        }
    }
}
