﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class Login : ConexionDBMysql
    {
        private string table = "empleado";

        public bool isUser(string usuario, string clave)
        {
            bool answer = false;
            string sql = $"SELECT * FROM " +
                    $"{this.table} " +
                $"WHERE " +
                        $"clave = MD5('{clave}') " +
                    $"AND " +
                        $"usuario= '{usuario}' " +
                    $"AND " +
                        $"eliminado = FALSE";
            var query = base.LeerDatos(sql);
            answer = query.Read();
            if (answer)
            {
                Sesion.ID = query.GetInt32("id");
                Sesion.Usuario = query.GetString("usuario");
                Sesion.Nombre = query.GetString("nombre");
                Sesion.Apellido = query.GetString("apellido");
            }
            return answer;
        }
    }
}
