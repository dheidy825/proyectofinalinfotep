﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class ConexionDBMysql
    {
        private static string Servidor = "127.0.0.1";
        private static string Usuario = "root";
        private static string Clave = "";
        private static string DB = "dbsistbebidas";
        private static string Puerto = "3306";
        private static MySql.Data.MySqlClient.MySqlConnection Conector = null;
        protected int UltimoIDInsetado = 0;

        protected MySql.Data.MySqlClient.MySqlConnection ConectarMysql()
        {
            string connectionstring = $@" datasource={Servidor};port={Puerto};
                                      username={Usuario};password={Clave};database={DB};";

            Conector = new MySql.Data.MySqlClient.MySqlConnection(connectionstring);
            Conector.Open();

            return Conector;
        }
        
        protected void DesconectarMysql()
        {
            Conector.Close();
        }

        protected void EjecutarAEI(string sql)
        {
            var myConn = ConectarMysql();
            var mySqlCommand = myConn.CreateCommand();
            mySqlCommand.CommandText = sql;
            mySqlCommand.ExecuteNonQuery();

            int.TryParse(mySqlCommand.LastInsertedId.ToString(), out UltimoIDInsetado);
            DesconectarMysql();

        }

        protected MySql.Data.MySqlClient.MySqlDataReader LeerDatos(string sql)
        {
            ConectarMysql();
            var mySqlCommand = Conector.CreateCommand();
            mySqlCommand.CommandText = sql;
            return mySqlCommand.ExecuteReader();
        }
    }
}
