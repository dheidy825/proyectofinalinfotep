﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class ClaDetalleFactura : ClaFactura
    {
        private int CantidadArticulos;
        private string table = "detallefactura";

        public ClaDetalleFactura(int idBebidas, int idEmpleado, int cantidadArticulos) : base(idBebidas, idEmpleado)
        {
            CantidadArticulos = cantidadArticulos;
        }

        public new void Agregar()
        {
            base.Agregar();
            string sql = $"INSERT INTO {this.table}(idfactura,cantidadarticulos) " +
                $"VALUE({base.idFactura},{this.CantidadArticulos})";
            base.EjecutarAEI(sql);
        }
    }
}
