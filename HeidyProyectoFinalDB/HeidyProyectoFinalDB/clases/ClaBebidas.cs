﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeidyProyectoFinalDB.clases
{
    class ClaBebidas : ConexionDBMysql
    {
        protected int id;
        private string tipo;
        private string nombre;
        private string categoria;
        private float precio;
        private string table = "bebidas";

        public ClaBebidas()
        {

        }

        protected ClaBebidas(string tipo, string nombre, string categoria, float precio)
        {
            this.tipo = tipo;
            this.nombre = nombre;
            this.categoria = categoria;
            this.precio = precio;
        }

        protected ClaBebidas(int id, string tipo, string nombre, string categoria, float precio)
        {
            this.id = id;
            this.tipo = tipo;
            this.nombre = nombre;
            this.categoria = categoria;
            this.precio = precio;
        }

        protected ClaBebidas(int id)
        {
            this.id = id;
        }

        public List<Bebidas> Consultar()
        {
            List<Bebidas> bebidas = new List<Bebidas>();
            string sql = $"SELECT * FROM {this.table} WHERE eliminado=FALSE";
            var query = base.LeerDatos(sql);
            while (query.Read())
            {
                bebidas.Add(
                    new Bebidas() {
                        ID = query.GetInt32("id"),
                        TipoBebidas = query.GetString("tipobebidas"),
                        NombreBebida = query.GetString("nombrebebidas"),
                        CategoriaBebida = query.GetString("categoriabebidas"),
                        PrecioBebida = query.GetFloat("Preciobebidas")
                    }  
                );
            }
            base.DesconectarMysql();
            return bebidas;
        }

        protected void Agregar()
        {
            string sql = $"INSERT INTO " +
                $"{this.table}" +
                    $"(tipobebidas,nombrebebidas,categoriabebidas,preciobebidas) " +
                $"VALUES " +
                    $"('{this.tipo}', '{this.nombre}', '{this.categoria}', '{this.precio}')";
            base.EjecutarAEI(sql);
            this.id = base.UltimoIDInsetado;
        }

        protected void Actualizar()
        {
            string sql = $"UPDATE " +
                    $"{this.table} " +
                $"SET " +
                    $"tipobebidas='{this.tipo}', " +
                    $"categoriabebidas='{this.categoria}', " +
                    $"preciobebidas='{this.precio}' " +
                $"WHERE " +
                    $"id={this.id}";
            base.EjecutarAEI(sql);
        }

        protected void Eliminar()
        {
            string sql = $"UPDATE {this.table} SET eliminado=TRUE WHERE id={this.id}";
            base.EjecutarAEI(sql);
        }

    }


    public class Bebidas
    {
        public int ID { set; get; }
        public string TipoBebidas { set; get; }
        public string NombreBebida { set; get; }
        public string CategoriaBebida { set; get; }
        public float PrecioBebida { set; get; }
    }

}
