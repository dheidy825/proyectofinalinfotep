-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-10-2020 a las 19:25:01
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsistbebidas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bebidas`
--

CREATE TABLE `bebidas` (
  `id` int(11) NOT NULL,
  `tipoBebidas` varchar(150) DEFAULT NULL,
  `nombreBebidas` varchar(150) DEFAULT NULL,
  `categoriaBebidas` varchar(150) DEFAULT NULL,
  `precioBebidas` float DEFAULT NULL,
  `Eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bebidas`
--

INSERT INTO `bebidas` (`id`, `tipoBebidas`, `nombreBebidas`, `categoriaBebidas`, `precioBebidas`, `Eliminado`) VALUES
(1, 'cerveza', 'presidente', 'normal', 125, 0),
(2, 'cerveza', 'brama', 'light', 100, 0),
(3, 'cerveza', 'smirnoft', 'normal', 200, 0),
(4, 'Romo', 'Brugal', 'Galon', 1500, 0),
(5, 'Romo', 'Barcelo gran añejo', 'normal', 1000, 0),
(6, 'Wisky', 'Chival', 'normal', 1800, 0),
(7, 'Hielo', 'Hielo', 'Funda', 60, 0),
(8, 'Jugo', '100% Rica', 'Grande', 100, 0),
(9, 'Jugo', 'Moth', 'Grande', 60, 0),
(10, 'Agua', 'Aloe vera', 'Grande', 150, 0),
(11, 'Agua', 'Agua Dasany', 'normal', 25, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallefactura`
--

CREATE TABLE `detallefactura` (
  `id` int(11) NOT NULL,
  `idFactura` int(11) DEFAULT NULL,
  `cantidadarticulos` int(11) DEFAULT NULL,
  `Eliminado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detallefactura`
--

INSERT INTO `detallefactura` (`id`, `idFactura`, `cantidadarticulos`, `Eliminado`) VALUES
(1, 1, 1, NULL),
(2, 2, 1, NULL),
(3, 4, 1, NULL),
(4, 9, 1, NULL),
(5, 10, 1, NULL),
(6, 11, 1, NULL),
(7, 12, 1, NULL),
(8, 13, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `usuario` varchar(150) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `apellido` varchar(150) DEFAULT NULL,
  `cedula` varchar(50) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `correo` varchar(150) DEFAULT NULL,
  `direccion` text DEFAULT NULL,
  `Eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `usuario`, `clave`, `nombre`, `apellido`, `cedula`, `telefono`, `correo`, `direccion`, `Eliminado`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Ricardo', 'Perez Perez', '102-4570221-1', '829-000-3651', 'turealchulito@gmail.com', 'Street 1445', 0),
(2, '', 'e10adc3949ba59abbe56e057f20f883e', 'Rafael', 'Minaya', '402-1261573-2', '829-377-5326', 'rafaga21@gmail.com', 'alma rosa 1ra', 1),
(3, 'heidy', 'e10adc3949ba59abbe56e057f20f883e', 'Heidy', 'De leon', '402-1221748-9', '829-270-1310', 'heidy@gmail.com', 'c/engombe', 0),
(4, 'dheidy', '25f9e794323b453885f5181f1b624d0b', 'maria', 'periel', '402-7777777-5', '805-555-5555', 'pepita@hotmail.com', 'calle manganagua', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `id` int(11) NOT NULL,
  `idBebidas` int(11) DEFAULT NULL,
  `idEmpleado` int(11) DEFAULT NULL,
  `Eliminado` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `idBebidas`, `idEmpleado`, `Eliminado`) VALUES
(9, 2, 3, 0),
(10, 2, 3, 0),
(11, 1, 3, 0),
(12, 1, 3, 0),
(13, 2, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `idEmpleado` int(11) DEFAULT NULL,
  `idBebidas` int(11) DEFAULT NULL,
  `CantidadProducto` int(11) DEFAULT NULL,
  `Nombreproveedor` varchar(150) DEFAULT NULL,
  `compañiaProveedora` varchar(150) DEFAULT NULL,
  `TelefonoProveedor` varchar(150) DEFAULT NULL,
  `direccionProveedor` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bebidas`
--
ALTER TABLE `bebidas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`id`),
  ADD KEY `factura_ibfk_1` (`idEmpleado`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bebidas`
--
ALTER TABLE `bebidas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `detallefactura`
--
ALTER TABLE `detallefactura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
